# Slugger
a function that accepts any number of strings as arguments and returns a single string with hyphens between.
[![slugger](slugger.png)](https://www.npmjs.com/package/@shani-sabag/slugger)

## installation
```
npm i @shani-sabag/slugger
```

## CommonJS
```javascript
const slugger = require('@shani-sabag/slugger'); 
```

## ES6 modules
```javascript
import { slugger } from '@shani-sabag/slugger'; 
```

## Usage
```javascript
slugger.slugger("I am", "a", "slugged string", "i contain no spaces")
```
output:
```javascript
"i-am-a-slugged-string-i-contain-no-spaces"
```