const slugger = require("./index.js");

/**
 * @describe [optional] - group of tests with a header to describe them
 */
 describe('testing slugger basic functionality', () => {
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
        expect(slugger.slugger("I am", "a", "slugged string", "i contain no spaces")).toEqual("I-am-a-slugged-string-i-contain-no-spaces");
    })
    /**
     * @test - unit test can use the 'test' syntax
     */
    // test('slugger can slug any number of spacy strings', () => {
    //     ...your code here
    //     expect(inputs).toEqual(output);
    // })
})