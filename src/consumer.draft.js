const marker = require("@ajar/marker");
const slugger = require("./index.js");

marker.blue(slugger.slugger("I am", "a", "slugged string", "i contain no spaces"));
