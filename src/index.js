function slugger(...args) {
    return args.map(str => {
        return str.split(" ").join("-");
    }).join("-");
}

module.exports = { slugger };